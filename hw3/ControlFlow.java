//checks that the analysis can process control flow appropriately

class ControlFlow
{
    public static void main (String[] args)
    {
        int a;
        int b;

        b = 0;
        a = 2;

        if (false) {
            a = a - 3;
        } else {
            a = a + 2;
        }
        a = a + 2;
        System.out.println(a);

        while (b < 1) {
            a = a * (1 - 2);
        }
        a = a + 7;
        System.out.println(a);
    }
}