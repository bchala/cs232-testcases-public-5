/* This shows how numerical analysis is flow sensitive and how classes extending classes
with the same method names can interfere with the analysis */

class Main {
    public static void main(String[] a){
        A f;
        int ret;      

        ret = 0;
        if (false) {
            f = new A();
        } else {
            f = new B();
        }

        ret = f.num(ret);
        System.out.println(ret);
     
    }
}

class A {
    public int num(int ret){
        return ret - 1;
    }
}

class B extends A {
    public int num(int ret){
        return ret + 1;
    }
}
