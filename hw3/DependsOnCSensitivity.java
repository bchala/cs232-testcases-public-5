// The nifty aspect of this program is that it ensures you are doing a context sensitive analysis 
// with depth 1 since input constraints of invoke must have this and a as the same object. 

class DependsOnCSensitivity {
    public static void main(String[] a){
    	A a = new A();
        System.out.println(a.invoke(a));
    }
}

class A {
	int field;

	public int invoke(A a) {
		field = 5;
		return a.b();
	}

	public int b() {
		return field;
	}
}
