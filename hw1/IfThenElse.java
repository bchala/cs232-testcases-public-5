class IfThenElse {
  public static void main(String[] a){
      System.out.println(new C().m(10));
  }
}

class C {
  P x;
  public int m(int i){
    if (i < 1)
        x = new P();
    else
        x = new Q();
    return x.f();
  }
}

class P {
  P y;
  public int f() {
    y = new R();
    return 2;
  }  
}

class Q extends P {
  public int f() {
    return 3;
  }  
}

class R extends Q {
  public int f() {
    return 3;
  }  
}
