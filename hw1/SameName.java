/* this example ensures that the call graph correctly determins which doB() is called,
 that the call graph construction can deal with a cycle, and that extraneous methods do not
 make it into the call graph */
class Example {
    public static void main(String[] args){
        A a;
        B b;
        C c;
        
        a = new A();
        b = new B();
        c = new C();
        a = b;
        System.out.println(a.doB());
    }
}

class A {
    public int doA(B b) {
        return b.doB();
    }
    
    public int doB() {
        return 0;
    }
}

class B extends A {
    public int doB() {
        return this.doA(new B());
    }
    
}

class C extends A {
    public int doB() {
        return 0;
    }
}

